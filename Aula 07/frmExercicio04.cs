﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_07
{
    public partial class frmExercicio04 : Form
    {
        int vItem;
        public frmExercicio04()
        {
            InitializeComponent();
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            float vQtde, vUnitario, vSubTotal;
            if (txtProduto.Text != "" && txtQtde.Text != "" && txtUnitario.Text != "")
            {
                vQtde = float.Parse(txtQtde.Text);
                vUnitario = float.Parse(txtUnitario.Text);
                vSubTotal = vQtde * vUnitario;
                grdItens.Rows.Add(vItem, txtProduto.Text, vQtde.ToString("N2"), vUnitario.ToString("N2"), vSubTotal.ToString("N2"));
                btnLimparCampos_Click(sender, e);
                txtProduto.Focus();
                vItem++;
            }
            else
            {
                MessageBox.Show("O preenchimento de todos os campos é obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtProduto.Focus();
            }
        }

        private void frmExercicio04_Load(object sender, EventArgs e)
        {
            vItem = 1;
        }

        private void btnLimparCampos_Click(object sender, EventArgs e)
        {
            txtProduto.Clear();
            txtQtde.Clear();
            txtUnitario.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btnLimparCampos_Click(sender, e);
            vItem = 1;
            grdItens.RowCount = 2;
            grdItens.Rows.Clear();
            txtTotalBruto.Clear();
            txtTotalLiquido.Clear();
            txtTotalLiquido.Clear();
            txtProduto.Focus();
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            int i;
            float bruto = 0, desconto, liquido;
            for (i = 0; i < grdItens.Rows.Count - 1; i++)
            {
                bruto += float.Parse(grdItens.Rows[i].Cells["subtotal"].Value.ToString());
            }
            txtTotalBruto.Text = bruto.ToString("N2");
            if (txtDesconto.Text == "")
            {
                txtDesconto.Text = "0";
            }
            desconto = float.Parse(txtDesconto.Text) / 100;
            liquido = bruto - (bruto * desconto);
            txtTotalLiquido.Text = liquido.ToString("N2");
        }
    }
}
