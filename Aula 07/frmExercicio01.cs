﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_07
{
    public partial class frmExercicio01 : Form
    {
        public frmExercicio01()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int valor, i, res;
            if (txtValor.Text != "")
            {
                valor = int.Parse(txtValor.Text);
                lblResultado.Text = "";
                for (i = 1; i <= 10; i++)
                {
                    res = valor * i;
                    lblResultado.Text = lblResultado.Text + valor + " X " + i.ToString() + " = " + res.ToString() + "\n";
                }
            }
            else{
                MessageBox.Show("Preencha todos os campos", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                lblResultado.Text = "";
                txtValor.Focus();
            }
        }
    }
}
