﻿namespace Aula_07
{
    partial class frmExercicio02
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBase = new System.Windows.Forms.Label();
            this.txtBase = new System.Windows.Forms.TextBox();
            this.lblExpoente = new System.Windows.Forms.Label();
            this.txtExpoente = new System.Windows.Forms.TextBox();
            this.lblRes = new System.Windows.Forms.Label();
            this.lblResultado = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblBase
            // 
            this.lblBase.Location = new System.Drawing.Point(12, 24);
            this.lblBase.Name = "lblBase";
            this.lblBase.Size = new System.Drawing.Size(62, 23);
            this.lblBase.TabIndex = 3;
            this.lblBase.Text = "Base";
            // 
            // txtBase
            // 
            this.txtBase.Location = new System.Drawing.Point(80, 24);
            this.txtBase.Name = "txtBase";
            this.txtBase.Size = new System.Drawing.Size(100, 20);
            this.txtBase.TabIndex = 2;
            // 
            // lblExpoente
            // 
            this.lblExpoente.Location = new System.Drawing.Point(12, 48);
            this.lblExpoente.Name = "lblExpoente";
            this.lblExpoente.Size = new System.Drawing.Size(62, 23);
            this.lblExpoente.TabIndex = 5;
            this.lblExpoente.Text = "Expoente";
            // 
            // txtExpoente
            // 
            this.txtExpoente.Location = new System.Drawing.Point(80, 48);
            this.txtExpoente.Name = "txtExpoente";
            this.txtExpoente.Size = new System.Drawing.Size(100, 20);
            this.txtExpoente.TabIndex = 4;
            // 
            // lblRes
            // 
            this.lblRes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblRes.Location = new System.Drawing.Point(80, 72);
            this.lblRes.Name = "lblRes";
            this.lblRes.Size = new System.Drawing.Size(100, 20);
            this.lblRes.TabIndex = 6;
            // 
            // lblResultado
            // 
            this.lblResultado.Location = new System.Drawing.Point(12, 72);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(62, 23);
            this.lblResultado.TabIndex = 7;
            this.lblResultado.Text = "Resultado";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(104, 100);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 8;
            this.btnCalcular.Text = "&Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // frmExercicio02
            // 
            this.AcceptButton = this.btnCalcular;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(197, 142);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.lblRes);
            this.Controls.Add(this.lblExpoente);
            this.Controls.Add(this.txtExpoente);
            this.Controls.Add(this.lblBase);
            this.Controls.Add(this.txtBase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio02";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercício 02";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBase;
        private System.Windows.Forms.TextBox txtBase;
        private System.Windows.Forms.Label lblExpoente;
        private System.Windows.Forms.TextBox txtExpoente;
        private System.Windows.Forms.Label lblRes;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Button btnCalcular;
    }
}