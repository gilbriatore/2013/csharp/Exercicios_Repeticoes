﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_07
{
    public partial class frmExtra : Form
    {
        public frmExtra()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int v1, v2, i, j, res;
            if (txtValor1.Text != "" && txtValor2.Text != "")
            {
                v1 = int.Parse(txtValor1.Text);
                v2 = int.Parse(txtValor2.Text);
                txtResultado.Clear();
                for (i = v1; i <= v2; i++)
                {
                    for (j = 1; j <= v2; j++)
                    {
                        res = i * j;
                        txtResultado.Text = txtResultado.Text + i.ToString() + " X " + j.ToString() + " = " + res.ToString() + Environment.NewLine;
                    }
                    txtResultado.Text = txtResultado.Text + Environment.NewLine;
                }
            }
            else{
                MessageBox.Show("Todos os campos são de preenchimento obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtValor1.Clear();
                txtValor2.Clear();
                txtResultado.Clear();
                txtValor1.Focus();
            }
        }
    }
}
