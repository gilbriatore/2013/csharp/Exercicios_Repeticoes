﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_07
{
    public partial class frmExercicio03 : Form
    {
        public frmExercicio03()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int valor, i, fat;
            if (txtValor.Text != "")
            {
                valor = int.Parse(txtValor.Text);
                lblResultado.Text = "";
                fat = 1;
                for (i = valor; i > 0; i--)
                {
                    fat *= i;
                    if (i != 1)
                    {
                        lblResultado.Text = lblResultado.Text + i.ToString() + " X ";
                    }
                    else
                    {
                        lblResultado.Text = lblResultado.Text + i.ToString() + " = ";
                    }
                }
                lblResultado.Text = lblResultado.Text + fat.ToString();
            }
            else{
                MessageBox.Show("Preencha todos os campos", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtValor.Clear();
                lblResultado.Text = "";
                txtValor.Focus();
            }
        }
    }
}
