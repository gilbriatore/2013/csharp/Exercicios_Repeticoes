﻿namespace Aula_07
{
    partial class frmExercicio04
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpDados = new System.Windows.Forms.GroupBox();
            this.txtUnitario = new System.Windows.Forms.TextBox();
            this.lblUnitario = new System.Windows.Forms.Label();
            this.txtQtde = new System.Windows.Forms.TextBox();
            this.lblQtde = new System.Windows.Forms.Label();
            this.txtProduto = new System.Windows.Forms.TextBox();
            this.lblProduto = new System.Windows.Forms.Label();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.btnLimparCampos = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.grpItens = new System.Windows.Forms.GroupBox();
            this.grdItens = new System.Windows.Forms.DataGridView();
            this.item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpFinalizar = new System.Windows.Forms.GroupBox();
            this.txtTotalLiquido = new System.Windows.Forms.TextBox();
            this.lblTotalLiquido = new System.Windows.Forms.Label();
            this.txtDesconto = new System.Windows.Forms.TextBox();
            this.lblDesconto = new System.Windows.Forms.Label();
            this.txtTotalBruto = new System.Windows.Forms.TextBox();
            this.lblTotalBruto = new System.Windows.Forms.Label();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.lblTexto = new System.Windows.Forms.Label();
            this.grpDados.SuspendLayout();
            this.grpItens.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdItens)).BeginInit();
            this.grpFinalizar.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDados
            // 
            this.grpDados.Controls.Add(this.txtUnitario);
            this.grpDados.Controls.Add(this.lblUnitario);
            this.grpDados.Controls.Add(this.txtQtde);
            this.grpDados.Controls.Add(this.lblQtde);
            this.grpDados.Controls.Add(this.txtProduto);
            this.grpDados.Controls.Add(this.lblProduto);
            this.grpDados.Location = new System.Drawing.Point(16, 16);
            this.grpDados.Name = "grpDados";
            this.grpDados.Size = new System.Drawing.Size(452, 104);
            this.grpDados.TabIndex = 0;
            this.grpDados.TabStop = false;
            this.grpDados.Text = "Dados do orçamento";
            // 
            // txtUnitario
            // 
            this.txtUnitario.Location = new System.Drawing.Point(72, 68);
            this.txtUnitario.Name = "txtUnitario";
            this.txtUnitario.Size = new System.Drawing.Size(128, 20);
            this.txtUnitario.TabIndex = 5;
            this.txtUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblUnitario
            // 
            this.lblUnitario.Location = new System.Drawing.Point(8, 68);
            this.lblUnitario.Name = "lblUnitario";
            this.lblUnitario.Size = new System.Drawing.Size(100, 20);
            this.lblUnitario.TabIndex = 4;
            this.lblUnitario.Text = "Unitário";
            // 
            // txtQtde
            // 
            this.txtQtde.Location = new System.Drawing.Point(72, 44);
            this.txtQtde.Name = "txtQtde";
            this.txtQtde.Size = new System.Drawing.Size(128, 20);
            this.txtQtde.TabIndex = 3;
            this.txtQtde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblQtde
            // 
            this.lblQtde.Location = new System.Drawing.Point(8, 44);
            this.lblQtde.Name = "lblQtde";
            this.lblQtde.Size = new System.Drawing.Size(100, 20);
            this.lblQtde.TabIndex = 2;
            this.lblQtde.Text = "Quantidade";
            // 
            // txtProduto
            // 
            this.txtProduto.Location = new System.Drawing.Point(72, 20);
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.Size = new System.Drawing.Size(368, 20);
            this.txtProduto.TabIndex = 1;
            // 
            // lblProduto
            // 
            this.lblProduto.Location = new System.Drawing.Point(8, 24);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.Size = new System.Drawing.Size(100, 20);
            this.lblProduto.TabIndex = 0;
            this.lblProduto.Text = "Produto";
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.Location = new System.Drawing.Point(476, 24);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(104, 28);
            this.btnAdicionar.TabIndex = 1;
            this.btnAdicionar.Text = "&Adicionar ítem";
            this.btnAdicionar.UseVisualStyleBackColor = true;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // btnLimparCampos
            // 
            this.btnLimparCampos.Location = new System.Drawing.Point(476, 56);
            this.btnLimparCampos.Name = "btnLimparCampos";
            this.btnLimparCampos.Size = new System.Drawing.Size(104, 28);
            this.btnLimparCampos.TabIndex = 2;
            this.btnLimparCampos.Text = "&Limpar Campos";
            this.btnLimparCampos.UseVisualStyleBackColor = true;
            this.btnLimparCampos.Click += new System.EventHandler(this.btnLimparCampos_Click);
            // 
            // btnNovo
            // 
            this.btnNovo.Location = new System.Drawing.Point(476, 92);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(104, 28);
            this.btnNovo.TabIndex = 3;
            this.btnNovo.Text = "&Novo orçamento";
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.button1_Click);
            // 
            // grpItens
            // 
            this.grpItens.Controls.Add(this.grdItens);
            this.grpItens.Location = new System.Drawing.Point(16, 136);
            this.grpItens.Name = "grpItens";
            this.grpItens.Size = new System.Drawing.Size(560, 184);
            this.grpItens.TabIndex = 4;
            this.grpItens.TabStop = false;
            this.grpItens.Text = "Ítens do orçamento";
            // 
            // grdItens
            // 
            this.grdItens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdItens.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.item,
            this.produto,
            this.quantidade,
            this.unitario,
            this.subtotal});
            this.grdItens.Location = new System.Drawing.Point(8, 20);
            this.grdItens.MultiSelect = false;
            this.grdItens.Name = "grdItens";
            this.grdItens.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdItens.Size = new System.Drawing.Size(540, 150);
            this.grdItens.TabIndex = 0;
            this.grdItens.TabStop = false;
            // 
            // item
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.item.DefaultCellStyle = dataGridViewCellStyle5;
            this.item.HeaderText = "Ítem";
            this.item.Name = "item";
            this.item.ReadOnly = true;
            this.item.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.item.Width = 50;
            // 
            // produto
            // 
            this.produto.HeaderText = "Produto";
            this.produto.Name = "produto";
            this.produto.ReadOnly = true;
            this.produto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.produto.Width = 150;
            // 
            // quantidade
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.quantidade.DefaultCellStyle = dataGridViewCellStyle6;
            this.quantidade.HeaderText = "Quantidade";
            this.quantidade.Name = "quantidade";
            this.quantidade.ReadOnly = true;
            this.quantidade.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.quantidade.Width = 80;
            // 
            // unitario
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.unitario.DefaultCellStyle = dataGridViewCellStyle7;
            this.unitario.HeaderText = "Unitário";
            this.unitario.Name = "unitario";
            this.unitario.ReadOnly = true;
            this.unitario.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // subtotal
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.subtotal.DefaultCellStyle = dataGridViewCellStyle8;
            this.subtotal.HeaderText = "SubTotal";
            this.subtotal.Name = "subtotal";
            this.subtotal.ReadOnly = true;
            this.subtotal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // grpFinalizar
            // 
            this.grpFinalizar.Controls.Add(this.txtTotalLiquido);
            this.grpFinalizar.Controls.Add(this.lblTotalLiquido);
            this.grpFinalizar.Controls.Add(this.txtDesconto);
            this.grpFinalizar.Controls.Add(this.lblDesconto);
            this.grpFinalizar.Controls.Add(this.txtTotalBruto);
            this.grpFinalizar.Controls.Add(this.lblTotalBruto);
            this.grpFinalizar.Location = new System.Drawing.Point(16, 332);
            this.grpFinalizar.Name = "grpFinalizar";
            this.grpFinalizar.Size = new System.Drawing.Size(352, 92);
            this.grpFinalizar.TabIndex = 5;
            this.grpFinalizar.TabStop = false;
            this.grpFinalizar.Text = "Finalizar orçamento";
            // 
            // txtTotalLiquido
            // 
            this.txtTotalLiquido.Location = new System.Drawing.Point(104, 52);
            this.txtTotalLiquido.Name = "txtTotalLiquido";
            this.txtTotalLiquido.ReadOnly = true;
            this.txtTotalLiquido.Size = new System.Drawing.Size(92, 20);
            this.txtTotalLiquido.TabIndex = 9;
            this.txtTotalLiquido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalLiquido
            // 
            this.lblTotalLiquido.Location = new System.Drawing.Point(8, 52);
            this.lblTotalLiquido.Name = "lblTotalLiquido";
            this.lblTotalLiquido.Size = new System.Drawing.Size(92, 20);
            this.lblTotalLiquido.TabIndex = 8;
            this.lblTotalLiquido.Text = "Total líquido";
            // 
            // txtDesconto
            // 
            this.txtDesconto.Location = new System.Drawing.Point(276, 20);
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.Size = new System.Drawing.Size(44, 20);
            this.txtDesconto.TabIndex = 7;
            this.txtDesconto.Text = "0";
            this.txtDesconto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDesconto
            // 
            this.lblDesconto.Location = new System.Drawing.Point(208, 24);
            this.lblDesconto.Name = "lblDesconto";
            this.lblDesconto.Size = new System.Drawing.Size(76, 20);
            this.lblDesconto.TabIndex = 6;
            this.lblDesconto.Text = "Desconto %";
            // 
            // txtTotalBruto
            // 
            this.txtTotalBruto.Location = new System.Drawing.Point(104, 24);
            this.txtTotalBruto.Name = "txtTotalBruto";
            this.txtTotalBruto.ReadOnly = true;
            this.txtTotalBruto.Size = new System.Drawing.Size(92, 20);
            this.txtTotalBruto.TabIndex = 5;
            this.txtTotalBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalBruto
            // 
            this.lblTotalBruto.Location = new System.Drawing.Point(8, 24);
            this.lblTotalBruto.Name = "lblTotalBruto";
            this.lblTotalBruto.Size = new System.Drawing.Size(92, 20);
            this.lblTotalBruto.TabIndex = 4;
            this.lblTotalBruto.Text = "Total bruto";
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.Location = new System.Drawing.Point(392, 340);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(184, 28);
            this.btnFinalizar.TabIndex = 6;
            this.btnFinalizar.Text = "&Finalizar orçamento";
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // lblTexto
            // 
            this.lblTexto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTexto.Location = new System.Drawing.Point(392, 380);
            this.lblTexto.Name = "lblTexto";
            this.lblTexto.Size = new System.Drawing.Size(180, 44);
            this.lblTexto.TabIndex = 7;
            this.lblTexto.Text = "Software desenvolvido para a prática de exercícios na linguagem C#";
            this.lblTexto.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmExercicio04
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 440);
            this.Controls.Add(this.lblTexto);
            this.Controls.Add(this.btnFinalizar);
            this.Controls.Add(this.grpFinalizar);
            this.Controls.Add(this.grpItens);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.btnLimparCampos);
            this.Controls.Add(this.btnAdicionar);
            this.Controls.Add(this.grpDados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio04";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercício 04";
            this.Load += new System.EventHandler(this.frmExercicio04_Load);
            this.grpDados.ResumeLayout(false);
            this.grpDados.PerformLayout();
            this.grpItens.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdItens)).EndInit();
            this.grpFinalizar.ResumeLayout(false);
            this.grpFinalizar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDados;
        private System.Windows.Forms.Label lblProduto;
        private System.Windows.Forms.TextBox txtProduto;
        private System.Windows.Forms.TextBox txtUnitario;
        private System.Windows.Forms.Label lblUnitario;
        private System.Windows.Forms.TextBox txtQtde;
        private System.Windows.Forms.Label lblQtde;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.Button btnLimparCampos;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.GroupBox grpItens;
        private System.Windows.Forms.DataGridView grdItens;
        private System.Windows.Forms.DataGridViewTextBoxColumn item;
        private System.Windows.Forms.DataGridViewTextBoxColumn produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotal;
        private System.Windows.Forms.GroupBox grpFinalizar;
        private System.Windows.Forms.TextBox txtTotalBruto;
        private System.Windows.Forms.Label lblTotalBruto;
        private System.Windows.Forms.TextBox txtDesconto;
        private System.Windows.Forms.Label lblDesconto;
        private System.Windows.Forms.TextBox txtTotalLiquido;
        private System.Windows.Forms.Label lblTotalLiquido;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Label lblTexto;
    }
}