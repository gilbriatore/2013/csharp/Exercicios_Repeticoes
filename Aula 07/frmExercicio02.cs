﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_07
{
    public partial class frmExercicio02 : Form
    {
        public frmExercicio02()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int b, ex, res, i;
            if (txtBase.Text != "" && txtExpoente.Text != "")
            {
                b = int.Parse(txtBase.Text);
                ex = int.Parse(txtExpoente.Text);
                lblRes.Text = "";
                res = 1;
                for (i = 1; i <= ex; i++)
                {
                    res = res * b;
                }
                lblRes.Text = res.ToString();
            }
            else{
                MessageBox.Show("Todos os campos são de preenchimento obrigatório","Alerta",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                txtBase.Clear();
                txtExpoente.Clear();
                lblRes.Text = "";
                txtBase.Focus();
            }
        }
    }
}
